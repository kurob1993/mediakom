import React from 'react';
import { IonGrid, IonRow, IonCol, IonContent } from '@ionic/react';
import './Home.css';

const Home: React.FC = () => {
   return (
      <IonContent>
         <IonGrid>
            <IonRow className="ion-text-center">
               <IonCol size="12">
                  <img 
                     alt="logo"
                     src="https://politeknikpgribanten.ac.id/wp-content/uploads/2017/05/cropped-logo-mobile-1.png" 
                  />
                  <h1>POLITEKNIK PGRI BANTEN</h1>
               </IonCol>
            </IonRow>
         </IonGrid>
      </IonContent>
   );
};

export default Home;
