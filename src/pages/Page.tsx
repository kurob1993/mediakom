import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import ExploreContainer from '../components/ExploreContainer';
import './Page.css';
import Home from './Home';

const Page: React.FC<RouteComponentProps<{ name: string; }>> = ({ match }) => {

  let Page = (select: string) => {
    switch (select) {
       case 'Home':
          return <Home />

       default:
          return <ExploreContainer name={select} />;
    }
 }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{match.params.name}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{ match.params.name }</IonTitle>
          </IonToolbar>
        </IonHeader>
        {/* <ExploreContainer name={match.params.name} /> */}
        {Page(match.params.name)}
      </IonContent>
    </IonPage>
  );
};

export default Page;
